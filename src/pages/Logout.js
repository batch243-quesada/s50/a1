import React, { useEffect, useContext } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext';

const Logout = () => {
    // window.location.href = '/login';

    const { unSetUser } = useContext(UserContext);
    
    useEffect(() => {
        unSetUser();
    }, [unSetUser])

    return (
        <Navigate to='/login' />
    )
}

export default Logout