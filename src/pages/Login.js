import React, { useEffect, useState, useContext } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Loading from './Loading';

const Login = () => {
    const [email, setEmail] = useState("");
    const [passwordOne, setPasswordOne] = useState("");
    const [isActive, setIsActive] = useState(false);
    // const [user, setUser] = useState(localStorage.getItem("email"));

    // Allows us to consume the User context object and its properties to use for user validation
    const {user, setUser} = useContext(UserContext);
    const [loading, setLoading] = useState(false);


    useEffect(() => {
        email !== "" && passwordOne !== ""
        ? setIsActive(true)
        : setIsActive(false)

    }, [email, passwordOne]);

    useEffect(() => {
        setLoading(true)
        setTimeout(() => {
          setLoading(false)
        }, 1000)
      }, [])

    const loginSubmit = e => {
        e.preventDefault();

        alert(`Welcome back!`);

        // Storing information in the local will make the data persistent even as the page is refreshed unlike unlike with the use of states where informations are reset when refreshing the page
        localStorage.setItem('email', email);

        // Set the global user state to have properties obtained from the local storage
        // though access to the user info can be done via the localStorage, this is necessary to update the user state which will help update the App component and rerend it to refreshing the page upon user login and log out
        setUser(localStorage.getItem("email"));

        // window.location.href = '/';
        setEmail("");
        setPasswordOne("");
    }

    return (
        <>
            {
                loading
                ? <Loading />
                : (user !== null)
                ?   <Navigate to='/' />
                :   <Container>
                        <Row className='w-100'>
                            <Col className='col-md-4 col-8 offset-md-4 offset-2'>
                                <Form className='bg-secondary p-3' onSubmit={loginSubmit}>
                                    <Form.Group className="mb-3" controlId="email">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control
                                        value={email}
                                        type="email"
                                        placeholder="Enter email"
                                        required
                                        onChange={e => setEmail(e.target.value)}/>
                                    <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                    </Form.Text>
                                    </Form.Group>
        
                                    <Form.Group className="mb-3" controlId="passwordOne">
                                        <Form.Label>Enter your desired password</Form.Label>
                                        <Form.Control
                                            value={passwordOne}
                                            type="password"
                                            placeholder="Password"
                                            required
                                            onChange={e => setPasswordOne(e.target.value)}/>
                                    </Form.Group>
        
                                    <Button variant="primary" type="submit" id="submitBtn" disabled={!isActive}>
                                        Login
                                    </Button>
                                </Form>
                            </Col>
                        </Row>
                    </Container>  
            }
        </>
          
    )
}

export default Login