import React, { useContext, useEffect } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { useState } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

const Register = () => {
    const [email, setEmail] = useState("");
    const [passwordOne, setPasswordOne] = useState("");
    const [passwordTwo, setPasswordTwo] = useState("");
    const [isActive, setIsActive] = useState(false);
    
    const { user, setUser } = useContext(UserContext);

    useEffect(() => {
        email !== "" && passwordOne !== "" && passwordTwo !== "" && passwordOne === passwordTwo
        ? setIsActive(true)
        : setIsActive(false)

    }, [email, passwordOne, passwordTwo]);

    const submitRegister = e => {
        e.preventDefault();

        alert(`Registered successfully!`);
        // setEmail("");
        // setPasswordOne("");
        // setPasswordTwo("");
        localStorage.setItem('email', email);
        setUser(localStorage.getItem("email"));

    }

    return (
        ( user )
        ? <Navigate to = "/" />
        : <Container>
            <Row className='w-100'>
                <Col className='col-md-4 col-8 offset-md-4 offset-2'>
                    <Form className='bg-secondary p-3' onSubmit={submitRegister}>
                        <Form.Group className="mb-3" controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            value={email}
                            type="email"
                            placeholder="Enter email"
                            required
                            onChange={e => setEmail(e.target.value)}/>
                        <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                        </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="passwordOne">
                            <Form.Label>Enter your desired password</Form.Label>
                            <Form.Control
                                value={passwordOne}
                                type="password"
                                placeholder="Password"
                                required
                                onChange={e => setPasswordOne(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="passwordTwo">
                            <Form.Label>Verify password</Form.Label>
                            <Form.Control
                                value={passwordTwo}
                                type="password"
                                placeholder="Password"
                                required
                                onChange={e => setPasswordTwo(e.target.value)}/>
                        </Form.Group>
                        
                        <Button variant="primary" type="submit" disabled={!isActive}>
                            Register
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
        
    )
}

export default Register