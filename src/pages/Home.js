import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Loading from './Loading';

// import Courses from './Courses'

const Home = () => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
    }, 1000)
  }, [])

  return (
    <>
      {
        loading
        ? <Loading />
        : <Container className='mb-5 homeContainer'> 
                <Banner />
                <Highlights />
                {/* <Courses/> */}
          </Container>
      }
    </>
    
  )
}

export default Home