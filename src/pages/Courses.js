import coursesData from "../data/courses";
import CourseCard from "../components/CourseCard";
import { Col, Container, Row } from "react-bootstrap";

const local = localStorage.getItem("email")

const Courses = () => {
  return (
    <Container>
      <Row className='courseRow w-100'>
        {coursesData.map(course => (
          <Col className="courseCard offset-md-4" md={4} key={course.id}>
              <CourseCard course = {course}/>
          </Col>
        ))}
      </Row>
    </Container>
  )
}

export default Courses